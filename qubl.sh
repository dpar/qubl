#!/bin/sh -eu
vmsfile="$HOME/.config/qubl/vms"
qublpath="/media/qubl"
sharepath="/media/share"
diskspath="$qublpath/disks"
temppath="$qublpath/templates"
overlayspath="/tmp/overlays"
qemuargs=

error() {
	printf 'Error: %s\n' "$1"
	exit 2
}

confirm() {
	read -p "$1. Continue? yes/no" -r conf
	case "$conf" in
	y|yes) return;;
	*) exit;;
	esac
}

addargs() {
	qemuargs="$qemuargs $@"
}

addgraphics() {
	addargs -device virtio-vga-gl
	addargs -display sdl,gl=on,window-close="$1",gl=on,grab-mod=lshift-lctrl-lalt
	addargs -monitor vc
}

addaudio() {
	case "$1" in
	none) return 0;;
	output) true;;
	duplex) true;;
	*) error "bad audio";;
	esac
	addargs -audiodev pa,id=snd0 -device intel-hda -device "hda-$1",audiodev=snd0
}

addnet() {
	local fwd=
	[ "$1" -gt 0 ] && fwd=",hostfwd=tcp:127.0.0.1:$1-:22"
	addargs -nic user,model=virtio-net-pci,id=net0"$fwd"
}

# name format snapshot
adddisk() {
	local cache='' format
	format="$2"
	[ "$format" = auto ] && format="${1##*.}"
	case "$format" in
	qcow2)	true;;
	raw)	cache=",cache=none";;
	*) error "bad disk format";;
	esac
	addargs -drive if=virtio,format="$format",file="$1",snapshot="$3""$cache"
}

addshare() {
	addargs -virtfs local,path="$1",mount_tag=share,security_model=none
}

templateimg() {
	[ "$#" -gt 1 ] || exit 1;
	mkdir -p "$overlayspath"
	local TP="$temppath/$2"
	local OP="$overlayspath/$2"
	case "$1" in
	create-overlay|co)
		[ -e "$OP" ] && confirm "Overlay file already exists"
		qemu-img create -o backing_file="$TP",backing_fmt=raw -f qcow2 "$OP";;
	discard|d)
		confirm "Discarding template overlay"
		rm "$OP";;
	commit|ci)
		qemu-img commit "$OP";;
	*)
		echo args; exit 1;
	esac
}

template() {
	local cmd="$1"
	shift
	case "$cmd" in
	img|i) templateimg "$@";;
	run|r)
		template="$1"
		shift
		addargs "$@"
		mem=4096
		smp=16
		fwd=
		addargs -enable-kvm -cpu host -name "$template-template" -nodefaults
		addargs -smp "$smp" -m "$mem"
		adddisk "$overlayspath/$template" qcow2 off
		addgraphics off
		addnet 0
		echo qemu-system-x86_64 $qemuargs
		exec qemu-system-x86_64 $qemuargs;;
	*)
		error "args"
	esac
}

setvmconf() {
	case "$1" in
	template)
		adddisk /dev/nvme0n1p5 raw on;;
#		case "$templateoverlay" in
#		true) adddisk "$overlayspath/$2" qcow2 on;;
#		*) adddisk "$temppath/$2" raw on;;
#		esac;;
	smp) smp="$2";;
	mem) mem="$2";;
	disk)
		adddisk "$2" "$3" "$disksnapshot";;
	fwd) fwd="$2";;
	graphics) graphics="$2";;
	audio) audio="$2";;
	custom) addargs "$2";;
	*) error "unkown key in vm conf"
	esac
}

readvmconf() {
	local section='' foundsection=false vm="$1"
	local key value
	[ -e "$vmsfile" ] || error "config file not found"
	while read key value; do
		case "$key" in
		\#*) continue;;
		?*) true;;
		*) continue;;
		esac
		if [ -z "$value" ]; then
			case "$key" in
			\[*\]) true;;
			*) error "bad section";;
			esac
			section="${key%]}"
			section="${section#[}"
			if [ "$section" = "$vm" ]; then
				foundsection=true
			fi
			continue
		fi
		if [ "$section" = "$vm" ]; then
			set -f
			setvmconf "$key" $value
			set +f
		fi
	done < "$vmsfile"
	[ "$foundsection" = false ] && error "vm $vm not found in config file"
	return 0
}

dispvm() {
	local rwfile
	local file=$(mktemp /tmp/disp.XXXXXX)
	qemu-img create -f raw "$file" 3G
	mkfs.ext4 -L rw -d "$qublpath/roots/alpine" "$file"
	rwfile="$file.qcow2"
	qemu-img convert -f raw "$file" -O qcow2 "$rwfile"
	rm "$file"
	echo "$rwfile"
}

runvm() {
	templateoverlay=''
	disksnapshot=off
	local name echoonly='' rwfile='' \
		cldisk=''
	while getopts "osed:ph:" name; do
		case "$name" in
		o) templateoverlay=true;;
		s) disksnapshot=on;;
		e) echoonly=true;;
		d) rwfile="$OPTARG";;
		h) sharepath="$OPTARG";;
		p) rwfile=$(dispvm);;
		?) error "bad option";;
		esac
	done
	shift $((OPTIND - 1))
	[ "$#" -ge 1 ] || error "missing vm name"
	template=''
	name="$1"
	shift
	addargs "$@"
	audio=none
	mem=2048
	smp=15
	fwd=0
	graphics=on
	addargs -name "$name"
	readvmconf "$name"
	[ -n "$rwfile" ] && adddisk "$rwfile" auto "$disksnapshot"
	addargs -enable-kvm -cpu host -nodefaults
	addargs -smp "$smp" -m "$mem"
	addaudio "$audio"
	addshare "$sharepath"
	case "$graphics" in
	on)	addgraphics $disksnapshot;;
	off) true;;
	*) error "bad graphics";;
	esac
	addnet "$fwd"
	echo qemu-system-x86_64 $qemuargs
	[ "$echoonly" != true ] && exec qemu-system-x86_64 $qemuargs
}

cmd="$1"
shift
case "$cmd" in
r|run) runvm "$@";;
t|temp|template) template "$@";;
*) error "args";;
esac
