#!/usr/bin/env python

import argparse, tomllib, sys, os
import subprocess

rtdir = os.getenv("XDG_RUNTIME_DIR") or "/tmp"


class Options:
    def __init__(self):
        smp = os.cpu_count() - 2
        if smp < 1:
            smp = 1
        self.default_smp = smp
        op = rtdir + "/qubl-overlays"
        self.overlays_path = op


def ensure_dir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


def confirm(msg):
    ans = input(f"{msg}, Continue? (y/n)")
    if ans != "y":
        sys.exit()


def error(msg):
    sys.stderr.write(f"Error: {msg}\n")
    sys.exit(1)


def get_config():
    with open(os.getenv("HOME") + "/.config/qubl/config.toml", "rb") as f:
        return tomllib.load(f)


class VM:
    def __init__(self, name, conf):
        self.conf = conf
        self.name = name

    def add_args(self, *a):
        self.args.extend(a)

    def add_disk(self, disk):
        fmt = disk["fmt"]
        match fmt:
            case "qcow2":
                cache = True
            case "raw":
                cache = False
            case _:
                error("Unrecoginzed disk format")
        drive = f"if=virtio,format={fmt},file={disk['file']},snapshot={disk['snapshot'] and 'on' or 'off'}"
        if not cache:
            drive += ",cache=none"
        self.add_args("-drive", drive)

    def add_disks(self):
        for d in self.conf["disks"]:
            self.add_disk(d)

    def add_audio(self):
        if "audio" not in self.conf:
            return
        audio = self.conf["audio"]
        assert audio in ("output", "duplex")
        self.add_args(
            "-audiodev",
            "pa,id=snd0",
            "-device",
            "intel-hda",
            "-device",
            f"hda-{audio},audiodev=snd0",
        )

    def add_net(self):
        try:
            if not self.conf["net"]["enabled"]:
                return
        except KeyError:
            pass
        nic = "user,model=virtio-net-pci,id=net0"
        try:
            fwd = self.conf["net"]["forward"]
        except KeyError:
            fwd = False
        if fwd:
            nic += f",hostfwd=tcp:127.0.0.1:{fwd}-:22"
        self.add_args("-nic", nic)

    def add_shares(self):
        for s in self.conf["shares"]:
            self.add_args(
                "-virtfs",
                f"local,path={s['path']},mount_tag={s['tag']},security_model=none",
            )

    def add_kernel(self):
        if "kernel" in self.conf:
            k = self.conf["kernel"]
            self.add_args("-kernel", k["file"])
            if "initrd" in k:
                self.add_args("-initrd", k["initrd"])
            if "append" in k:
                self.add_args("-append", k["append"])

    def add_graphics(self):
        window_close = self.conf["window_close"] and "on" or "off"
        self.add_args("-device", "virtio-vga-gl")
        self.add_args(
            "-display",
            f"sdl,gl=on,window-close={window_close},gl=on,grab-mod=lshift-lctrl-lalt",
        )
        self.add_args("-monitor", "vc")

    def get_args(self):
        self.args = []
        self.add_args("-name", self.name)
        self.add_args("-enable-kvm", "-cpu", "host", "-nodefaults")
        self.add_args("-smp", str(self.conf["smp"]), "-m", str(self.conf["mem"]))
        self.add_audio()
        self.add_disks()
        self.add_shares()
        self.add_graphics()
        self.add_net()
        self.add_kernel()
        return self.args


def exec_qemu(args):
    qemu = "qemu-system-x86_64"
    os.execvp(qemu, [qemu] + args)


def run_cmd(cliargs):
    conf = get_config()
    vmconf = conf["vms"][cliargs.vm]
    if cliargs.disable_networking:
        vmconf["net"] = dict(enabled=False)
    vmconf["shares"] = [conf["default-share"]]
    if "smp" not in vmconf:
        vmconf["smp"] = options.default_smp
    if "mem" not in vmconf:
        vmconf["mem"] = 2048
    for d in vmconf["disks"]:
        d["snapshot"] = cliargs.snapshot
    if "template" in vmconf:
        tconf = conf["templates"][vmconf["template"]]
        if cliargs.use_template_overlay:
            tempdisk = dict(
                file=options.overlays_path + "/" + vmconf["template"], fmt="qcow2"
            )
        else:
            tempdisk = tconf["disk"]
        tempdisk["snapshot"] = True
        vmconf["disks"].insert(0, tempdisk)
        if "kernel" not in vmconf and "kernel" in tconf:
            vmconf["kernel"] = tconf["kernel"]
    vmconf["window_close"] = cliargs.snapshot
    vm = VM(cliargs.vm, vmconf)
    qemu = "qemu-system-x86_64"
    if cliargs.echo:
        print(" ".join(vm.get_args()))
    else:
        exec_qemu(vm.get_args())


def run_template_cmd(cliargs):
    conf = get_config()
    tconf = conf["templates"][cliargs.template]
    vmconf = {}
    if "kernel" in tconf:
        vmconf["kernel"] = tconf["kernel"]
    vmconf["smp"] = options.default_smp
    vmconf["mem"] = 4096
    disk = tconf["disk"]
    disk["snapshot"] = False
    vmconf["disks"] = [disk]
    vmconf["window_close"] = False
    vmconf["shares"] = []
    vm = VM(f"template-{cliargs.template}", vmconf)
    exec_qemu(vm.get_args())


def create_overlay_cmd(cliargs):
    conf = get_config()
    tempconf = conf["templates"][cliargs.template]
    tpath = tempconf["disk"]["file"]
    tfmt = tempconf["disk"]["fmt"]
    ofile = options.overlays_path + "/" + cliargs.template
    ensure_dir(options.overlays_path)
    if os.path.exists(ofile):
        confirm("Overlay file already exists")
    subprocess.run(
        [
            "qemu-img",
            "create",
            "-o",
            f"backing_file={tpath},backing_fmt={tfmt}",
            "-f",
            "qcow2",
            ofile,
        ]
    )


def commit_cmd(cliargs):
    ofile = options.overlays_path + "/" + cliargs.template
    subprocess.run(["qemu-img", "commit", ofile])


def main():
    arg_parser = argparse.ArgumentParser()
    main_cmd_subparser = arg_parser.add_subparsers(
        title="main cmd", dest="main_cmd", required=True
    )
    templates_cmd_parser = main_cmd_subparser.add_parser("templates", aliases=["t"])
    templates_cmd_subparser = templates_cmd_parser.add_subparsers(
        title="template cmd", dest="template_cmd", required=True
    )
    run_template_cmd_parser = templates_cmd_subparser.add_parser("run", aliases=["r"])
    run_template_cmd_parser.add_argument("template")
    create_overlay_cmd_parser = templates_cmd_subparser.add_parser(
        "create-overlay", aliases=["co"]
    )
    create_overlay_cmd_parser.add_argument("template")
    commit_cmd_parser = templates_cmd_subparser.add_parser("commit", aliases=["ci"])
    commit_cmd_parser.add_argument("template")
    run_cmd_parser = main_cmd_subparser.add_parser("run", aliases=["r"])
    run_cmd_parser.add_argument("--snapshot", "-s", action="store_true")
    run_cmd_parser.add_argument("--disable-networking", "-n", action="store_true")
    run_cmd_parser.add_argument("--echo", "-e", action="store_true")
    run_cmd_parser.add_argument("--use-template-overlay", "-o", action="store_true")
    run_cmd_parser.add_argument("vm")
    args = arg_parser.parse_args()
    global options
    options = Options()
    match args.main_cmd:
        case "templates" | "t":
            match args.template_cmd:
                case "run" | "r":
                    run_template_cmd(args)
                case "create-overlay" | "co":
                    create_overlay_cmd(args)
                case "commit" | "ci":
                    commit_cmd(args)
                case _:
                    assert False
        case "run" | "r":
            run_cmd(args)
        case _:
            assert False


if __name__ == "__main__":
    main()
